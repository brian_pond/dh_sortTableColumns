## Datahenge: Sort Table Columns

A tool for improving the position of ERPNext tables' columns.

### Motivation

While working on a MySQL data migration in DBeaver, I discovered I was spending a **lot** of time searching for columns.

For example, I had to find a column named '*net_amount*' and  apply an `IFNULL`.  But my `SELECT` query is a hundred lines long.  Where is my column located?  Is it near the beginning, middle, or end of my query?

* Yes, I could use "search".  But as a veteran SQL developer, that quickly becomes tedious.  Sometimes I just want to use my eyes and the scrollbar.
* Yes, I could sort my SELECT statement.  But that only solves the problem *this* time.  What about the next query or project?
* Anytime I auto-generate an INSERT statement, the columns will be unsorted.
* Anytime I auto-generate a SELECT statement, the columns will be unsorted.
* Anytime I browse the table with an IDE, the columns will be unsorted.

However, what if I physically *modify* my table, and "re-sort" all the columns?  It turns out that with MySQL, you can! 

### MySQL Syntax
Here are the 2 ways you can alter a column's position in MySQL:

```mysql
ALTER TABLE table_name MODIFY columnB varchar(20) FIRST;  -- Make 'columnB' the First column in the Table.
ALTER TABLE table_name MODIFY columnB varchar(20) AFTER columnA;  -- Place 'columnB' immediately after 'columnA'
```

The ERPNext database has ~800 tables, and tens of thousands of columns.  So to make this work, I needed to automate those
SQL commands.  So I wrote Python code that would:

1.  Given any table, examine the columns, and applies the proper sort order.
2.  Given an entire database, repeat this process for each table.


### Installation: Development
```bash
git clone https://gitlab.com/brian_pond/dh_sortTableColumns.git
cd dh_sortTableColumns
source dev_scripts/make_venv.bash  # Creates a Python virtual environment
pip install -e .
```

### Usage
#### Prepare a Configuration
For this program to work, you need to create a configuration file named `config.toml`.
This file will contain the name of your database.  A connection string (username, password, host)

It also allows for custom sorting rules.  I -always- want the ERPNext column named "name" to be at the beginning,
followed by the "docstatus" column.  Also, I want certain columns to always appear at the end of the table: _assign, _comments, _likedby, etc.

Below is a sample `config.toml`

```toml
title = "Configuration for dh_sortTableColumns"
database_name = "my_database_name"

# Target Database
target_conn_string = "mysql+pymysql://user_name:password@127.0.0.1:3306/my_database_name"

[sort_order]
name = -2
docstatus = -1
_assign = 1
_comments = 2
_liked_by = 3
_user_tags = 4
idx = 5
creation = 6
owner = 7
modified = 8
modified_by = 9
```

Make sure to edit appropriate values for **database_name** and **target_conn_string**.

#### How to Run the Program
The program has a small Click command line interface.
```
sort_table_columns --help
```

There are 2 commands you can run:
1. Sort columns in a *single* MySQL table:

```bash
sort_table_columns single ./config.toml tabCustomer
```

2. Sort all columns in *all* tables in the database:

```bash
sort_table_columns all ./config.toml
```

# Public packages
import click
import sys
from dh_sortTableColumns import getPackageVersion


def print_version(ctx, param, value):
	"""Prints current application version."""
	if not value or ctx.resilient_parsing:
		return
	else:
		click.echo(f"Version {getPackageVersion()}")
		ctx.exit()


def cli_entry_point():
	""" Called by Setuptools entry_points()"""
	if len(sys.argv) > 1 and sys.argv[1] == "--help":
		print(click.Context(main_group).get_help())
		return
	main_group()


@click.group()
@click.option('--version', is_flag=True, is_eager=True, callback=print_version, expose_value=False)
def main_group(bench_path='.'):
	""" Main group of commands """
	pass


@click.command('single')
@click.argument('config_path', type=str)
@click.argument('table_name', type=str)
def sort_single(config_path, table_name):
	"Sort columns in a single MySQL table."
	from dh_sortTableColumns.sort import Datahenge_SortTableColumns as dhst
	result = dhst(config_path).sort_single_table(table_name)
	if result is True:
		click.echo(f"Re-sorted MySQL table '{table_name}' successfully.\n")


@click.command('all')
@click.argument('config_path', type=str)
def sort_all(config_path):
	"Sort columns in all MySQL tables."
	from dh_sortTableColumns.sort import Datahenge_SortTableColumns as dhst
	result = dhst(config_path).sort_all_tables()
	if result[1] == 0:
		click.echo("Re-sorted MySQL tables successfully.\n")


main_group.add_command(sort_single)
main_group.add_command(sort_all)

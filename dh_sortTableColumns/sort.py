# Standard Python library packages
import logging
import os
import pathlib
import pprint
import sqlalchemy
import sys
import toml
# Local Modules:
import dh_sortTableColumns.overloads  # noqa F401


# Configure Python logging
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)


class Datahenge_SortTableColumns:
	""" Re-sort columns in existing MySQL tables."""
	def __init__(self, configfile_path):
		self.config_dict = self.__load_configDict_from_toml(configfile_path)
		self.target_engine = sqlalchemy.create_engine(self.config_dict['target_conn_string'])
		self.prettier = pprint.PrettyPrinter(indent=4)

		if not test_sql_connections([self.target_engine]):
			raise Exception("Cannot establish connection to SQL target.")

	def sort_all_tables(self):
		success = 0
		error = 0
		inspector = sqlalchemy.inspect(self.target_engine)
		for table_name in inspector.get_table_names():
			print(f"Sorting table: {table_name}")
			if self.sort_single_table(table_name):
				success = success + 1
			else:
				error = error + 1

		if success:
			logger.info(f"{success} tables were sorted successfully.")
		if error:
			logger.error(f"{error} tables failed due to errors.")
		return (success, error)

	def sort_single_table(self, _targetTableName):
		""" Single table """
		if not validate_table_exists(self.target_engine, _targetTableName):
			logger.error(f"No such table '{_targetTableName}' exists in the database.")
			return False
		try:
			inspector = sqlalchemy.inspect(self.target_engine)
			columns = inspector.get_columns(_targetTableName)  # List of Dictionaries.
			column_names = [column['name'] for column in columns]
			ordered_column_names = sortListByRules(column_names, self.config_dict['sort_order'])

			# Build and Execute sort commands
			for index in range(0, len(ordered_column_names) - 1):
				sql_command = ""
				this_column_name = ordered_column_names[index]
				logger.debug(f"Repositioning table: {_targetTableName}, column: {this_column_name}")
				# Use dictionary comprehension to find our column's Dict of attributes.
				column_dict = next((foo for foo in columns if foo['name'] == this_column_name), None)
				if not column_dict:
					raise Exception(f"Unable to find metadata for column: {this_column_name}")
				if index == 0:
					sql_command = self.__build_sql_command(_targetTableName, column_dict, None)
				else:
					sql_command = self.__build_sql_command(_targetTableName, column_dict, ordered_column_names[index - 1])

				self.__execute_sql_command(sql_command)

		except Exception as e:
			raise(e)
			print(f"--> Unhandled Exception  while processing table \'{_targetTableName}\'")
			print('Error type is:', e.__class__.__name__)
			return False

		# print(f"Completed column sort for SQL table: '{_targetTableName}'")
		return True

	# ------
	# Private Functions
	# ------
	def __build_sql_command(self, table_name, column_dict, prior_column_name):
		""" Generates an SQL statement to alter a column's position"""

		# Step 1: Base Command
		sql_command = f"ALTER TABLE `{table_name}` MODIFY `{column_dict['name']}` "

		# Step 2: Add data type
		try:
			type_as_string = column_dict['type'].to_string()
			if type_as_string is None:
				raise Exception(f"Missing overloaded function 'to_string' for SQLAlchemy type { type(column_dict['type'])}")
		except Exception:
			print(f"Missing overloaded function 'to_string' for SQLAlchemy type { type(column_dict['type'])}")
			sys.exit(1)
		sql_command = sql_command + type_as_string

		# Step 3: Add a default value, if applicable.
		if column_dict['default'] is not None:
			sql_command = sql_command + " DEFAULT " + column_dict['default']

		# Step 4: Add non-nullable, if applicable.
		if column_dict['nullable'] is False:
			sql_command = sql_command + " NOT NULL"

		# Step 5: Add prior column name, or FIRST
		if prior_column_name:
			sql_command = sql_command + f" AFTER `{prior_column_name}`;"
		else:
			sql_command = sql_command + " FIRST;"

		# Step 6: Certain characters break Python (%).  Need to escape them.
		sql_command = sql_command.replace("%", "%%")
		return sql_command

	def __execute_sql_command(self, command):
		""" Connect to SQL database, and execute a SQL command. """
		try:
			sql_conn = self.target_engine.connect()
			trans = sql_conn.begin()
			resultproxy = sql_conn.execute(command)  # noqa F841
			# Not sure what to do with resultproxy for an ALTER TABLE statement. :/
			trans.commit()
		except Exception as e:
			trans.rollback()
			print(f"(DEBUG) SQL Statement:\n{command}")
			raise e
		finally:
			sql_conn.close()

	def __load_configDict_from_toml(self, _path_to_config):
		path_to_config = pathlib.Path(_path_to_config)
		if not path_to_config.exists() or not path_to_config.is_file():
			raise Exception(f"TOML configuration file {path_to_config} does not exist.")
		# TODO: Schema Validation
		return toml.load(path_to_config)  # returns a Python Dictionary.


# ----------------
# Module Functions
# ----------------
def resultproxy_to_list(result_proxy):
	"""Returns a List of results"""
	rowproxy_list = result_proxy.fetchall()  # sqlalchemy.engine.result.ResultProxy
	ret = []
	if len(rowproxy_list) == 0:
		return ret

	# Results from sqlalchemy are returned as a List of tuples.
	# This code converts List of tuples ---> List of dicts
	for row_number, row in enumerate(rowproxy_list):
		ret.append({})
		for column_number, value in enumerate(row):
			ret[row_number][row.keys()[column_number]] = value
	return ret


def sortListByRules(valueList, rulesDict):
	"""Sort a list of values based on Rules."""
	# Validate that List contains unique values.

    # Validate datatypes in argument List
	for val in valueList:
		if not isinstance(val, str):
			raise Exception("List does not exclusively contain Strings.")
	try:
		valueTuple = tuple(sorted(valueList))  # noqa F841
	except Exception:
		logger.error("Values to-be-sorted are not unique.")
		raise ValueError

	def inline_sort(an_element):
		"""Returns an unsigned integer that is the Sort Order"""
		index_orig = valueTuple.index(an_element)
		offset = None

		if an_element in rulesDict:
			offset = rulesDict[an_element]
			if offset > 0:
				index_new = len(valueTuple) + offset
			elif offset < 0:
				index_new = offset  # Negative offset indicates an absolute position.
			else:
				raise Exception("Unexpected offset value of zero(0) found in configuration.")
		else:
			# Keep current position.
			index_new = index_orig
		return index_new

	result = sorted(valueTuple, key=inline_sort)
	return result


def test_sql_connections(engine_list):
	"""Given a List of SQL Alchemy engines, verify connections can be made."""
	def __test_conn(description, _engine):
		ret = False
		try:
			connection = _engine.connect()
			result_proxy = connection.execute('SELECT 1;')
			if result_proxy.fetchone() == (1,):
				logger.info(f"  {description} connection established; success.")
				ret = True
			else:
				raise Exception(f"{description} connection expected tuple (1,)")
		except Exception:
			pass
		finally:
			connection.close()
			return ret

	result = True
	try:
		for engine in engine_list:
			result = result & __test_conn('Target', engine.connect())
	except Exception as e:
		logger.warning("Failed to establish connection with a MySQL database:")
		logger.warning("    * Check your firewall.")
		logger.warning("    * Check your connection string in 'config.toml'")
		logger.warning(e)
	return result


def validate_table_exists(engine, table_name):
	""" Verify a SQL table exists in a SQLAlchemy engine."""
	try:
		alchemy_table = sqlalchemy.schema.Table(table_name,
		                                        sqlalchemy.schema.MetaData(),
		                                        autoload=True,  # noqa: F841
		                                        autoload_with=engine)
		if alchemy_table.exists:
			return True
		else:
			logger.error(f"Target table '{table_name}' does not exist.")
			return False
	except sqlalchemy.exc.NoSuchTableError:
		logger.error(f"Target table '{table_name}' does not exist.")
		return False
	except Exception as e:
		print('Unhandled exception in validate_target_table()')
		print('Error type is:', e.__class__.__name__)
		raise e

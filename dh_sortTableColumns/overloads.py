# We need to Overload some SQLAlchemy casses.
#
# For most alchemy types, you can just str(type) and get a good value.
# Strings are the exception, because they also have collation data.
# We don't care about modifying collations when reordering columns.
# So we just need the first portion, the type and length.

from sqlalchemy.dialects.mysql.types import TEXT as sqlAlchemy_text  # noqa E402
from sqlalchemy.dialects.mysql.types import LONGTEXT as sqlAlchemy_longtext  # noqa E402
from sqlalchemy.dialects.mysql.types import VARCHAR as sqlAlchemy_varchar  # noqa E402
from sqlalchemy.sql.sqltypes import DATE as sqlAlchemy_date  # noqa E402
from sqlalchemy.dialects.mysql.types import INTEGER as sqlAlchemy_integer  # noqa E402
from sqlalchemy.dialects.mysql.types import BIGINT as sqlAlchemy_biginteger  # noqa E402
from sqlalchemy.dialects.mysql.types import DATETIME as sqlAlchemy_datetime  # noqa E402
from sqlalchemy.dialects.mysql.types import DECIMAL as sqlAlchemy_decimal  # noqa E402
from sqlalchemy.dialects.mysql.types import TIME as sqlAlchemy_time  # noqa E402


def to_string_default(self):
	""" This function works fine for most non-string SQLAlchemy types"""
	ret = str(self)
	return ret


sqlAlchemy_date.to_string = to_string_default
sqlAlchemy_time.to_string = to_string_default
sqlAlchemy_integer.to_string = to_string_default
sqlAlchemy_datetime.to_string = to_string_default
sqlAlchemy_decimal.to_string = to_string_default
sqlAlchemy_biginteger.to_string = to_string_default


def to_string_string_types(self):
	""" String names have collation, but we don't need to modify."""
	# print(f"My type is: {type(self)}")
	ret = str(self).split(' ')
	return ret[0]


sqlAlchemy_text.to_string = to_string_string_types
sqlAlchemy_longtext.to_string = to_string_string_types
sqlAlchemy_varchar.to_string = to_string_string_types

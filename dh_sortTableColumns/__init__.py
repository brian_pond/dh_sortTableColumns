# Global variables
__version__ = "0.0.1"  # Setuptools examines this during package installation.
__version_info__ = tuple(map(int, __version__.split('.')))


def getPackageVersion():
	return __version__

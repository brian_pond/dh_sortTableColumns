#!/bin/bash
#
# This script must be "sourced".
# One reason is because it activates a Python Virtual environment in the current shell (not a subshell)
#
# Developer Note: To exit gracefully (whether script was executed directly or sourced):
#     return -1 2> /dev/null || exit -1
#
# Caller will pass a path to the Python executable:

if [[ "$1" == "" ]]; then
    echo -e "\nUsage:"
    echo -e "    source make_venv.bash <path_to_python> --reinstall (optional)\n"
    return -1 2> /dev/null || exit -1
fi

python_path=$1
#
# Session Variables
# Get path to this script's directory.
THISDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get an absolute path to 'pyvenv.d'
envdir=$(realpath "$THISDIR/../pyvenv.d")

echo -e "\n---Python Virtual Environment Builder---\n"
# If argument '--reinstall', remove existing Python virtual environment.
if [[ "$2" = '--reinstall' ]] ; then
	rm -rf ~/.cache/pip
    if [[ "$VIRTUAL_ENV" != '' ]]; then
        deactivate
    fi
    rm -rf $envdir
    echo -e "\u2713 Removed previous Python virtual environment."
fi

# If no Python virtual environment exists, create it.
if [ ! -d "$envdir" ]; then
    bash -c "$python_path -m venv $envdir"
    echo -e "\u2713 Created new Python Virtual environment ($envdir) ..."
    echo -e "[global]\ndisable-pip-version-check = True" > "$envdir/pip.conf"
fi

# Activate Python virtual environment, if not already active.
if [[ "$VIRTUAL_ENV" == '' ]]; then
  source "$envdir/bin/activate"
  echo -e "\u2713 Activated Python virtual environment."
fi

python_version=$(python --version)
echo -e "\u2713 Using $python_version at $(which python)"

# Default pip logfile should be in the venv directory
export PIP_LOG_FILE="$VIRTUAL_ENV/pip_log.txt"

# Cleanup Variables
unset envdir
unset python_path

# Update to latest version of pip.
echo -e "Upgrading pip ..."
pip install --quiet --upgrade pip
echo -e "Installing additional Python tools ..."
pip install --quiet --upgrade setuptools wheel flake8 flake8-tabs pytest twine
echo -e "\u2713 Script complete.\n"
